package com.github.lucapino.confluence.rest.client.migration.confluence;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.lucapino.confluence.rest.client.api.ClientFactory;
import com.github.lucapino.confluence.rest.client.api.ContentClient;
import com.github.lucapino.confluence.rest.client.api.UserClient;
import com.github.lucapino.confluence.rest.client.impl.ClientFactoryImpl;
import com.github.lucapino.confluence.rest.client.migration.plon.PlonModel;
import com.github.lucapino.confluence.rest.client.migration.plon.ReadPlonJson;
import com.github.lucapino.confluence.rest.core.api.RequestService;
import com.github.lucapino.confluence.rest.core.api.domain.UserBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.BodyBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.ContentBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.LabelBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.LabelsBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.StorageBean;
import com.github.lucapino.confluence.rest.core.api.domain.content.VersionBean;
import com.github.lucapino.confluence.rest.core.api.domain.space.SpaceBean;
import com.github.lucapino.confluence.rest.core.api.misc.ContentStatus;
import com.github.lucapino.confluence.rest.core.api.misc.ContentType;
import com.github.lucapino.confluence.rest.core.impl.APIUriProvider;

public class ConfluenceContentFromPlon {
	
    private final ExecutorService executorService;
    private final RequestService requestService;
    private final APIUriProvider apiConfig;

	 private static Logger log = LoggerFactory.getLogger(ConfluenceContentFromPlon.class);

	    public ConfluenceContentFromPlon(ExecutorService executorService, RequestService requestService, APIUriProvider apiConfig) {
	        this.executorService = executorService;
	        this.requestService = requestService;
	        this.apiConfig = apiConfig;
	    }

	    public void run(String spaceId, String pageId) {
	    	
	        //read plon json
	    	List<PlonModel> plonList = ReadPlonJson.readStream(spaceId, pageId);
	    		    	
	        ClientFactory factory = new ClientFactoryImpl(executorService, requestService, apiConfig);
	        UserClient userClient = factory.getUserClient();
	        try {
	            Future<UserBean> anonymousUser = userClient.getCurrentUser();
	            UserBean user = anonymousUser.get();
	            
	            log.info("--------------->>>> Current user: " + user.getDisplayName());
	        } catch (URISyntaxException | InterruptedException | ExecutionException e) {
	            e.printStackTrace();
	        }
	        
	        final ContentClient contentClient = factory.getContentClient();
	        
    		SpaceBean spaceBean = new SpaceBean();
    		spaceBean.setKey(spaceId);
        	
    		for (PlonModel plonPage: plonList) {
	        	
    			//main page
	        	createUpdatePage (contentClient, plonPage, spaceBean, spaceId);
	        	
	        	try {
		        	//children pages
		        	if (null!=plonPage.getChildren() && !plonPage.getChildren().isEmpty()) { // && !(plonPage.getChildren().get(0) instanceof PlonModel)
			        	for (PlonModel child: plonPage.getChildren()) {
			        		createUpdatePage(contentClient, child, spaceBean, spaceId);
			        	}
		        	}
	        	} catch (Exception e) {
	        		e.printStackTrace();
	        	}
	        } 
	    }
	    
	    private ContentBean createBodyContent(ContentClient contentClient, PlonModel plonPage, ContentBean contentBean, boolean create) {
	    	ContentBean changedContentBean = null;
	    	
	    	//creates body content from the plone
    		StorageBean storageBean = new StorageBean();
    		storageBean.setValue(plonPage.getText());
    		storageBean.setRepresentation("storage");
									
			if (create) {
				
				//please don't set a space key, otherwise the update won't function. It will say, that it cannot update
				//a space-key
				
	    		//adds body content to the body
	    		BodyBean bodyBean = new BodyBean();
	    		bodyBean.setStorage(storageBean);
				
				//adds body to the page
				contentBean.setBody(bodyBean);
				
				try {					
					//creates the new page
					changedContentBean = contentClient.createContent(contentBean).get();
					
				} catch (InterruptedException | ExecutionException e) {
					log.error("---->>> CAN NOT CREATE PAGE = "+contentBean.getTitle());
					e.printStackTrace();
				}
				
			} else {
				
				//updates the existing page
				try {
					
					ContentBean page = contentClient.getContentById(contentBean.getId(), 0, Arrays.asList("version")).get();
					
					//the body can not be updated without a new version
					VersionBean versionBean = new VersionBean();
					versionBean.setNumber(page.getVersion().getNumber()+1);
					contentBean.setVersion(versionBean);
					
					contentBean.getBody().setStorage(storageBean);
					
					changedContentBean = contentClient.updateContent(contentBean).get();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
    		
    		return changedContentBean;
	    }

	    private void createUpdatePage(ContentClient contentClient, PlonModel plonPage, SpaceBean spaceBean, String spaceId) {
    		ContentBean createdPage = null;
    		
    		try {
    			//if a page with this title is found, use it and update it
    			createdPage = contentClient.getContent(ContentType.PAGE, spaceId, plonPage.getTitle(), ContentStatus.CURRENT, null, Arrays.asList("body.storage"), 0, 0).get().getResults().get(0);
				
				createdPage = createBodyContent(contentClient, plonPage, createdPage, false);
				
			} catch (Exception e) {
				
				//if no such page is found, create a new page
				log.info("--> PAGE DOESN'T EXISTS, will create it: " + plonPage.getTitle());
				
				ContentBean pageBean = new ContentBean();
				pageBean.setTitle(plonPage.getTitle());
				pageBean.setType("page");
				
				VersionBean versionBean = new VersionBean();
				versionBean.setNumber(0);
				
				//needed for update of the page body
				pageBean.setVersion(versionBean);
				
				//add the page to the space
				pageBean.setSpace(spaceBean);
													
				createdPage = createBodyContent(contentClient, plonPage, pageBean, true);
			}
    		
    		log.info("LABELS section START -------------------------------------------------------------");
    		
    		//labels need a createPage ID, that's why add them after the creation
    		if (null!=createdPage && null!=createdPage.getId()) {
    			
        		//check if labels exist
				try {
					LabelsBean labelsBean = contentClient.getLabels(createdPage).get();
					
	        		List<LabelBean> labelBeanExistingList = labelsBean.getResults();
	        		
	        		//get labels from plon	        		
	        		List<String> labels = new ArrayList<String>(Arrays.asList(plonPage.getPhysicalPath().split("/")));
	        		//remove ""/Plone/interner-service
	        		labels.remove(0);
	        		labels.remove(0);
	        		labels.remove(0);
	        		        		
	        		for (String label:labels) {
	        			
	        			LabelBean labelBean = new LabelBean("global", label);
	        			
	        			if (!labelBeanExistingList.contains(labelBean)) {
	        				labelBeanExistingList.add(labelBean);
	        			}
	        		}
	        		        
	    			//add labels to the page executes the post request
	        		contentClient.addLabels(createdPage, labelBeanExistingList);
					
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
    		} else {
    			log.error("---> createdPage.getId() is NULL");
    		}
			
    		log.info("LABELS section END -------------------------------------------------------------");
	    }
}

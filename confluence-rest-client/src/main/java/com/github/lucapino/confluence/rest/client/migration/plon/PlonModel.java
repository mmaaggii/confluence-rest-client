package com.github.lucapino.confluence.rest.client.migration.plon;

import java.util.List;

public class PlonModel {
	
    public boolean acquireCriteria;
    private List<PlonModel> children;
    private String[] contributors;
    private String creationDate;//?
    private String[] creators;//?
    private boolean customView;
    private String[] customViewFields;
    private String description;//
    private String effectiveDate;
    private boolean excludeFromNav;
    private String expirationDate;
    private String siteId;//nicht!
    private int itemCount;
    private String language;
    private boolean limitNumber;
    private String location;//
    private String modificationDate;
    private String physicalPath;//--> baumstruktur
    private String portalType;
    private String[] relatedItems;
    private String rights;
    public String[] subject;
    public String text;//--> body!!!!
    public String title;//--> pageTitle

    public PlonModel() {
    }

    public PlonModel(String title, String text, String[] subject) {
        this.title = title;
        this.text = text;
        this.subject = subject;
    }
    
    @Override
    public String toString() {
        return "PlonSite{" + "title=" + title + ", text=" + text + ", subject=" + subject.toString() +'}';
    }
    
	public boolean isAcquireCriteria() {
		return acquireCriteria;
	}

	public void setAcquireCriteria(boolean acquireCriteria) {
		this.acquireCriteria = acquireCriteria;
	}

	public List<PlonModel> getChildren() {
		return children;
	}

	public void setChildren(List<PlonModel> children) {
		this.children = children;
	}

	public String[] getContributors() {
		return contributors;
	}

	public void setContributors(String[] contributors) {
		this.contributors = contributors;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String[] getCreators() {
		return creators;
	}

	public void setCreators(String[] creators) {
		this.creators = creators;
	}

	public boolean getCustomView() {
		return customView;
	}

	public void setCustomView(boolean customView) {
		this.customView = customView;
	}

	public String[] getCustomViewFields() {
		return customViewFields;
	}

	public void setCustomViewFields(String[] customViewFields) {
		this.customViewFields = customViewFields;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isExcludeFromNav() {
		return excludeFromNav;
	}

	public void setExcludeFromNav(boolean excludeFromNav) {
		this.excludeFromNav = excludeFromNav;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean isLimitNumber() {
		return limitNumber;
	}

	public void setLimitNumber(boolean limitNumber) {
		this.limitNumber = limitNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getPhysicalPath() {
		return physicalPath;
	}

	public void setPhysicalPath(String physicalPath) {
		this.physicalPath = physicalPath;
	}

	public String getPortalType() {
		return portalType;
	}

	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}

	public String[] getRelatedItems() {
		return relatedItems;
	}

	public void setRelatedItems(String[] relatedItems) {
		this.relatedItems = relatedItems;
	}

	public String getRights() {
		return rights;
	}

	public void setRights(String rights) {
		this.rights = rights;
	}

	public String[] getSubject() {
		return subject;
	}

	public void setSubject(String[] subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}

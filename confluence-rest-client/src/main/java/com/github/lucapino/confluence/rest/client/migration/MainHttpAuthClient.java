package com.github.lucapino.confluence.rest.client.migration;

import static com.github.lucapino.confluence.rest.core.impl.HttpAuthProperties.BASE_URL;
import static com.github.lucapino.confluence.rest.core.impl.HttpAuthProperties.PASSWORD;
import static com.github.lucapino.confluence.rest.core.impl.HttpAuthProperties.USER;

import java.net.URI;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.cli.*;
import org.apache.commons.cli.ParseException;

import com.github.lucapino.confluence.rest.client.migration.confluence.ConfluenceContentFromPlon;
import com.github.lucapino.confluence.rest.core.impl.APIAuthConfig;
import com.github.lucapino.confluence.rest.core.impl.APIUriProvider;
import com.github.lucapino.confluence.rest.core.impl.HttpAuthRequestService;

public class MainHttpAuthClient {
	
	// local machine settings
	private static String baseurl = "http://localhost:1990";
	private static String addurl = "/confluence";
	private static String username = "admin";
	private static String password = "admin";
	private static String spaceId = "0"; //<<<-----
	private static String pageId = "0"; //<<<-----

    public static void main(String[] args) throws Exception {
    	
    	//put in a comment if you are working on the local machine
    	//cmdRunWithOptions (args);
    	
        new MainHttpAuthClient().run();
    }
    
    private static void cmdRunWithOptions (String[] args) {
    	Options options = new Options();

        Option baseurlOption = new Option("bu", "baseurl", true, "base_url of confluence instance");
        baseurlOption.setRequired(false);
        options.addOption(baseurlOption);
        
        Option addurlOption = new Option("au", "addurl", true, "additional path to the base url of the confluence instance, local machine is /confluence, productive is /");
        addurlOption.setRequired(false);
        options.addOption(addurlOption);
        
        Option usernameOption = new Option("u", "username", true, "username");
        usernameOption.setRequired(false);
        options.addOption(usernameOption);
        
        Option passwordOption = new Option("p", "password", true, "password");
        passwordOption.setRequired(false);
        options.addOption(passwordOption);
        
        Option spaceIdOption = new Option("spaceid", "spaceId", false, "confluence space id");
        passwordOption.setRequired(false);
        options.addOption(spaceIdOption);
        
        Option pageIdOption = new Option("pageid", "pageId", false, "confluence page id");
        passwordOption.setRequired(false);
        options.addOption(pageIdOption);
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            
            //Default values are the productive settings, if nothing else inputed on start
            baseurl = cmd.getOptionValue("baseurl", ""); //<<<-----
            addurl = cmd.getOptionValue("addurl", "/");
            username = cmd.getOptionValue("username", ""); //<<<-----
            password = cmd.getOptionValue("password", ""); //<<<-----
            spaceId = cmd.getOptionValue("spaceid", ""); //<<<-----
            pageId = cmd.getOptionValue("pageid", "0"); //<<<-----
            
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }
    }

    private void run() {
        try {
            ExecutorService executorService = Executors.newFixedThreadPool(100);

            APIAuthConfig conf = loadAuthConfig();
            HttpAuthRequestService requestService = new HttpAuthRequestService();
            requestService.connect(new URI(conf.getBaseUrl()), conf.getUser(), conf.getPassword());

            APIUriProvider uriProvider = new APIUriProvider(new URI(conf.getBaseUrl() + addurl));
            
            new ConfluenceContentFromPlon(executorService, requestService, uriProvider).run(spaceId, pageId);
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private APIAuthConfig loadAuthConfig() throws Exception {
        //HashMap<String, String> defaultProperties = getDefaultProperties();
        //PropertiesFileStore store = new PropertiesFileStore("target/example-httpauth.properties", defaultProperties);
        //Map<String, String> props = store.getProperties();
        //return new APIAuthConfig(props.get(BASE_URL), props.get(USER), props.get(PASSWORD));
        //mi added for testing
        return new APIAuthConfig(baseurl, username, password);
    }

    private HashMap<String, String> getDefaultProperties() {
        HashMap<String, String> defaultProperties = new HashMap<>();
        
        defaultProperties.put(BASE_URL, baseurl);
        defaultProperties.put(USER, username);
        defaultProperties.put(PASSWORD, password);
        return defaultProperties;
    }
}

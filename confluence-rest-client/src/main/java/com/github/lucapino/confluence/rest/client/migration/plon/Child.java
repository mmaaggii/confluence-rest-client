package com.github.lucapino.confluence.rest.client.migration.plon;

public class Child {
	public String field;
	public String id;
	public String operator;
	public String portalType;
	public boolean reversed;
	public String titleValue;
	
    public Child() {
    }

    public Child (String field, String id, String operator, String portalType, boolean reversed, String titleValue) {
        this.field = field;
        this.id = id;
        this.operator = operator;
        this.portalType = portalType;
        this.reversed = reversed;
        this.titleValue = titleValue;
    }

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPortalType() {
		return portalType;
	}

	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}

	public boolean isReversed() {
		return reversed;
	}

	public void setReversed(boolean reversed) {
		this.reversed = reversed;
	}

	public String getTitleValue() {
		return titleValue;
	}

	public void setTitleValue(String titleValue) {
		this.titleValue = titleValue;
	}
}

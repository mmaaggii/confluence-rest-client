package com.github.lucapino.confluence.rest.client.migration.plon;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class ReadPlonJson {
	
    private static final String FILENAME = "plonData.json";
    
    /**
     * This is a mixed implementation based on stream and object model. The JSON
     * file is read in stream mode and each object is parsed in object model.
     * With this approach we avoid to load all the object in memory and we are only
     * loading one at a time.
     * 
     * Source: https://www.acuriousanimal.com/blog/2015/10/23/reading-json-file-in-stream-mode-with-gson
     * @return 
     */
    public static List<PlonModel> readStream(String spaceId, String pageId) {
    	
    	ConcurrentHashMap<String, PlonModel> plonMap = new ConcurrentHashMap<String, PlonModel>();
    	List<PlonModel> plonList = new ArrayList<PlonModel>();
    	
        try {
        	
        	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        	InputStream stream = classloader.getResourceAsStream(FILENAME);
        	
            JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
            Gson gson = new GsonBuilder().create();
            
            

            // Read file in stream mode
            reader.beginArray();
            
            while (reader.hasNext()) {
            	
                // Read data into object model
                PlonModel plon = gson.fromJson(reader, PlonModel.class);
                
                if (!plon.getTitle().isEmpty()) {
                	
                    if (!pageId.equals("0")) {
                    	if (plon.getSiteId().equalsIgnoreCase(pageId))
                    		plonList.add(plon);
                    		break;
                    }
                    
                    else {
                    	plonList.add(plon);
                    }
                }
            }
            
            reader.close();
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ReadPlonJson.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadPlonJson.class.getName()).log(Level.SEVERE, null, ex);
        }
        
		return plonList;
    }
}
